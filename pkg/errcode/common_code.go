package errcode

var (
	Success                   = NewError(0, "成功")
	ServerError               = NewError(10000000, "伺服器錯誤")
	InvalidParams             = NewError(10000001, "參數錯誤")
	NotFound                  = NewError(10000002, "Not Found")
	UnauthorizedAuthNotExist  = NewError(10000003, "授權失敗，not found AppKey and AppSecret")
	UnauthorizedTokenError    = NewError(10000004, "授權失敗，Token error")
	UnauthorizedTokenTimeout  = NewError(10000005, "授權失敗，Token timeout")
	UnauthorizedTokenGenerate = NewError(10000006, "授權失敗，Token fail")
	TooManyRequests           = NewError(10000007, "請求過多")
)
