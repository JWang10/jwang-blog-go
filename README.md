# Blog-Go

* [DB Schema](#db-schema)
    * [創建標籤表](#創建標籤表)
    * [創建文章表](#創建文章表)
    * [創建文章標籤關聯表](#創建文章標籤關聯表)
* [路由設計](#路由設計)
    * [標籤管理](#標籤管理)
    * [文章管理](#文章管理)
* [Demo](#demo)



## DB Schema
> 可參考 `docs/DB-schema.md`
### 創建標籤表
- 表字段為標籤名稱、狀態及公共字段

### 創建文章表
- 表字段為文章標題、封面、內容摘要及公共字段

### 創建文章標籤關聯表
- 主要用於記錄文章與標籤之間的`1:N`關聯關係


## 路由設計

### 標籤管理

| 功能         | HTTP method | route     |
|--------------|-------------|:----------|
| 新增標籤     | POST        | /tags     |
| 刪除指定標籤 | DELETE      | /tags/:id |
| 更新指定標籤 | PUT         | /tags/:id  |
| 獲取標籤列表 | GET         | /tags     |

### 文章管理

| 功能         | HTTP method | route     |
|--------------|-------------|:----------|
| 新增文章     | POST        | /articles     |
| 刪除指定文章 | DELETE      | /articles/:id |
| 更新指定文章 | PUT         | /articles/:id  |
| 獲取指定文章 | GET         | /articles/:id  |
| 獲取文章列表 | GET         | /articles     |

## Demo

- Step 1：數據庫、數據模型，以及接口方法設計和handler處理方法
![demo1.png](assets/demo1.png)