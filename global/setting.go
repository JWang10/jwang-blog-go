package global

import (
	"jwang-blog-go/pkg/logger"
	"jwang-blog-go/pkg/setting"
)

var (
	// Configs
	ServerSetting   *setting.ServerSettingS
	AppSetting      *setting.AppSettingS
	DatabaseSetting *setting.DatabaseSettingS
	// Logger
	Logger *logger.Logger
)
